# 010Editor-keygen
A keygen for 010Editor

## How does it work
* TODO

## How to build
In console:

```
g++ -std=c++11 main.cpp -o 010Editor-keygen.exe
```

## How to use
Just run it.

Example:
```
E:\Github\010Editor-keygen>010Editor-keygen.exe
Input your name(English Only): DoubleLabyrinth
47AD-E1AC-D789-D133-2114

E:\Github\010Editor-keygen>
```

__NOTICE:__  
The key generated will expired after 2020-06-23.